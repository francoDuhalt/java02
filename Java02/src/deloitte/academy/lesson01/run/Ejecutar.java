package deloitte.academy.lesson01.run;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import deloitte.academy.lesson01.array.Buscar;
import deloitte.academy.lesson01.array.Curso;
import deloitte.academy.lesson01.array.Obtener;
import deloitte.academy.lesson01.array.Ordenar;
/**
 * 
 * @author Franco Duhalt
 *
 */
public class Ejecutar {
	
	private static final Logger LOGGER = Logger.getLogger(Ejecutar.class.getName());
	public static void main(String[] args)throws SecurityException, IOException {
		
		int[] marks = {125, 132, 95, 116, 110, 110};
		String[] flags = {"true", "false", "false", "false", "true"};
		String[] places = {"veracruz, Queretaro, Mexico"};
		String findingWord = "veracruz";
		int arr_size = marks.length; 
		List<ArrayList<String>> vals = Ordenar.orderFlag(flags);
		ArrayList<String> courseName = new ArrayList<String>();
		ArrayList<String> courseDate = new ArrayList<String>();
		String newCourse = "JavaScript";
		String newDate = "06/03/2020";
		
		courseName.add("Java");
		courseDate.add("04/03/2020");
        
		
		LOGGER.info("The highest score is " + Obtener.maximum(marks));
		LOGGER.info("The smallest score is " + Obtener.minimum(marks));
		LOGGER.info("The repeat number is " + Obtener.repeat(marks, arr_size));
		LOGGER.info(Buscar.search(places, findingWord));
		LOGGER.info(vals.toString());
		LOGGER.info(Curso.add(courseName, newCourse).toString());
		LOGGER.info(Curso.orderList(courseName).toString());
		LOGGER.info(Curso.add(courseDate, newDate).toString());
		LOGGER.info(Curso.orderList(courseDate).toString());
		LOGGER.info(Curso.remove(courseName, newCourse).toString());
		LOGGER.info(Curso.remove(courseDate, newDate).toString());
		
		
		
		
		
		
		
		

	}

}
