package deloitte.academy.lesson01.array;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The function of this class is to separate in to list if it's true or false
 * @author Franco Duhalt
 *
 */

public class Ordenar {
	
	private static final Logger LOGGER = Logger.getLogger(Obtener.class.getName());
	/**
	 * This Method go through an array to find the true or falses statements and then adding them to the correct list
	 * @param flags
	 * @return truFlag List and falseFlag List
	 */
	
	public static List<ArrayList<String>> orderFlag(String[] flags) { 
		
		List<ArrayList<String>> listResultado = new ArrayList<>();
		
		List<String> trueFlag = new ArrayList();
		List<String> falseFlag = new ArrayList();
        
        try {
        	for (String flag : flags) {
				if (flag.equals("true")) {
					trueFlag.add(flag);
				}else {
					falseFlag.add(flag);
				}
			}
        }catch(Exception e) {
        	LOGGER.log(Level.SEVERE, "Exception occur", e);
        }
        

        listResultado.add((ArrayList<String>) trueFlag);
        listResultado.add((ArrayList<String>) falseFlag);
	    return listResultado;
	}
}
