package deloitte.academy.lesson01.array;

import java.util.List;

public class ListaBoolean {

	public List<Boolean> falsos;
	public List<Boolean> verdaderos;
	
	public ListaBoolean() {
		// TODO Auto-generated constructor stub
	}
	
	
	public List<Boolean> getFalsos() {
		return falsos;
	}
	public void setFalsos(List<Boolean> falsos) {
		this.falsos = falsos;
	}
	public List<Boolean> getVerdaderos() {
		return verdaderos;
	}
	public void setVerdaderos(List<Boolean> verdaderos) {
		this.verdaderos = verdaderos;
	}
	
	
	
}
