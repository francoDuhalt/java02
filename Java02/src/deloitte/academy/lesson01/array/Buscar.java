package deloitte.academy.lesson01.array;

import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * The function of this class is to find a word in a string.
 * @author Franco Duhalt
 *
 */

public class Buscar {
	private static final Logger LOGGER = Logger.getLogger(Buscar.class.getName());
	/**
	 * This method works with a for to go through an array then uses an if to find the search word
	 * @param array of words
	 * @param word to be found
	 * @return search word
	 */
	public static String search(String[] search, String word) {

		String result = "";

		try {
			for(int i = 0; i<= search.length - 1; i++){
		       
				
				if (search[i].contains(word)) {
		    	   result="Palabra encontrada";
		       }else {
		    	   result="No se encontr� la palabra";
		       }
		    }
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}

		return result;
	}
}
