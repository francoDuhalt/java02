package deloitte.academy.lesson01.array;

import java.util.ArrayList;
import java.util.Collections;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class add, sort, and remove items form an array
 * @author Franco Duhalt
 *
 */
public class Curso {
	private static final Logger LOGGER = Logger.getLogger(Buscar.class.getName());

	/**
	 * This methods adds an item to the array.
	 * @param array
	 * @param data
	 * @return add new data to the array
	 */
	public static ArrayList add(ArrayList<String> courseData, String data) {

		try {
			courseData.add(data);
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}

		return courseData;
	}
	/**
	 * This method sort the items in the array
	 * @param courseData
	 * @return order array
	 */

	public static ArrayList orderList(ArrayList<String> courseData) {

		try {
			Collections.sort(courseData);
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}

		return courseData;
	}
	/**
	 * This method remove and item form the array.
	 * @param courseData
	 * @param data
	 * @return remove from array
	 */

	public static ArrayList remove(ArrayList<String> courseData, String data) {

		try {
			courseData.remove(data);
		} catch (Exception e) {
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}

		return courseData;
	}

}
