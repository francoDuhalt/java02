package deloitte.academy.lesson01.array;

import java.util.logging.Logger;
import java.util.logging.Level;

/**
 * This Class gets the highest, the smallest and the repeat value in an array
 * @author Franco Duhalt
 *
 */
public class Obtener {
	private static final Logger LOGGER = Logger.getLogger(Obtener.class.getName());
	/**
	 * The Method goes through an array then uses and if to find the highest value.
	 * @param array of numbers
	 * @return highest value
	 */
	public static int maximum(int[] numbers) {

		int maxSoFar = numbers[0];

		try{
			for (int num : numbers) {
				if (num > maxSoFar) {
					maxSoFar = num;
				}
			}
		}catch(Exception e){
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}
		

		return maxSoFar;
	}

	/**
	 * The Method goes through an array then uses and if to find the smallest value.
	 * @param array of numbers
	 * @return smallest value
	 */

	public static int minimum(int[] numbers) {

		int minSoFar = numbers[2];

		try{
			for (int num : numbers) {
				if (num < minSoFar) {
					minSoFar = num;
				}
			}
		}catch(Exception e){
			LOGGER.log(Level.SEVERE, "Exception occur", e);
		}

		return minSoFar;
	}
	/**
	 * The Method goes through an array then uses and if to find the repeat value.
	 * @param array
	 * @param array size
	 * @return repeat numbers
	 */

	public static int repeat(int arr[], int size) { 
		int repeatNumber = 0;
		
        int i, j; 
        
        try {
        	for (i = 0; i < size; i++){ 
  	          for (j = i + 1; j < size; j++){ 
  	             if (arr[i] == arr[j])  {
  	               repeatNumber = arr[i];   
  	            } 
  	          } 
  	        }
        }catch(Exception e) {
        	LOGGER.log(Level.SEVERE, "Exception occur", e);
        }
        

	    return repeatNumber;
	}

}
